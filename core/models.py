from django.db import models

class Cafe(models.Model):

    name = models.CharField(max_length= 250)
    instagram = models.CharField(max_length=250)
    latitude = models.DecimalField(max_digits=22, decimal_places=16, help_text="latitude")
    longitude = models.DecimalField(max_digits=22, decimal_places=16, help_text="longitude")
    phone_number = models.CharField(max_length=50) 
    address = models.TextField()
    open_time = models.TextField()
    
    last_update = models.DateField()

    create_date = models.DateTimeField(auto_now_add=True)

    Y = '0'
    N = '1'
    U = '2'

    tags = (
        (Y, ("yes")),
        (N, ("no")),
        (U, ("undefined"))
    )

    takeaway = models.CharField(max_length = 1, choices= tags)
    salon = models.CharField(max_length = 1, choices= tags)
    smoke = models.TextField()

    scrap_url = models.URLField()
